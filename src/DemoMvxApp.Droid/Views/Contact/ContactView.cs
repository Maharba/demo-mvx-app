using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using DemoMvxApp.Core.ViewModels.Contact;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.ViewModels;
using AlertDialog = Android.App.AlertDialog;

namespace DemoMvxApp.Droid.Views.Contact
{
    [Activity(Theme = "@style/AppTheme",
            WindowSoftInputMode = SoftInput.AdjustPan | SoftInput.StateHidden)]
    public class ContactView : BaseActivity<ContactViewModel>
    {
        #region Activity LifeCycle
        private Android.Support.V7.Widget.Toolbar _toolbar;
        protected override int ActivityLayoutId => Resource.Layout.contact_view;

        #endregion

        private IMvxInteraction _dialogInteraction;

        public IMvxInteraction DialogInteraction
        {
            get => _dialogInteraction;
            set
            {
                if (_dialogInteraction != null)
                {
                    _dialogInteraction.Requested -= OnDialogInteractionRequested;
                }

                _dialogInteraction = value;
                _dialogInteraction.Requested += OnDialogInteractionRequested;
            }
        }

        private void OnDialogInteractionRequested(object sender, EventArgs e)
        {
            var alertDiag = new AlertDialog.Builder(this);
            alertDiag.SetTitle("Error");
            alertDiag.SetMessage("Password must consist of the following:\n- Mixture of letters and numbers\n- 5 and 12 characters.\n- Must not contain any sequence of characters immediately followed by the same sequence.");
            alertDiag.SetNeutralButton("Ok", (senderAlert, args) => {});
            Dialog diag = alertDiag.Create();
            diag.Show();
        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            this.MenuInflater.Inflate(Resource.Menu.contact_view_menu, menu);

            return base.OnCreateOptionsMenu(menu);
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            CreateBindings();
        }

        private void CreateBindings()
        {
            var set = this.CreateBindingSet<ContactView, ContactViewModel>();
            set.Bind(this).For(view => view.DialogInteraction).To(viewModel => viewModel.DialogInteraction).OneWay();
            set.Apply();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.save_contact:
                    ViewModel.SaveCommand.ExecuteAsync();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
