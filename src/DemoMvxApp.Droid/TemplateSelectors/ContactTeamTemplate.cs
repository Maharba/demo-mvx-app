using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DemoMvxApp.Core.Models;
using DemoMvxApp.Core.Models.Nodes;
using MvvmCross.Droid.Support.V7.RecyclerView.ItemTemplates;

namespace DemoMvxApp.Droid.TemplateSelectors
{
    class ContactTeamTemplate : MvxTemplateSelector<ContactNode>
    {
        public override int GetItemLayoutId(int fromViewType) => fromViewType;

        protected override int SelectItemViewType(ContactNode forItemObject)
        {
            switch (forItemObject.NodeType)
            {
                case ContactNodeType.None:
                    return 0;
                case ContactNodeType.TeamMember:
                    // return a resource layout
                    return Resource.Layout.contact_row_view;
                default:
                    throw new NotImplementedException(nameof(ContactTeamTemplate) + " - contact node not found.");
            }
        }
    }
}
