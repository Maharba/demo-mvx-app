namespace DemoMvxApp.Core.Models.Nodes
{
    public class ContactNode : BaseNode
    {
        public ContactNodeType NodeType { get; set; }

        public ContactNode(ContactNodeType nodeType, object tValue)
        {
            NodeType = nodeType;
            Value = tValue;
        }

        public ContactNode()
        {
        }
    }

    public enum ContactNodeType
    {
        None = 0,
        TeamMember = 1
    }
}
