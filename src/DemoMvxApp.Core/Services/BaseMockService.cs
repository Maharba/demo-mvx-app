using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using DemoMvxApp.Core.Helpers;
using DemoMvxApp.Core.Interfaces;

namespace DemoMvxApp.Core.Services
{
    public class BaseMockService
    {
        protected Dictionary<string, object> ObjectCache;
        protected readonly ILoggingService LoggingService;

        public BaseMockService(ILoggingService loggingService)
        {
            LoggingService = loggingService;
            ObjectCache = new Dictionary<string, object>();
        }

        protected async Task<T> GetObjectFromFile<T>(string filename)
        {
            T obj = default(T);
            try
            {

                if (ObjectCache.ContainsKey(filename))
                {
                    return (T)ObjectCache[filename];
                }
                else
                {
                    obj = await FileDeserializerHelper.SerializeJsonFromFile<T>(filename);
                    ObjectCache.Add(filename, obj);
                }
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }
            return obj;
        }

        protected async Task<IList<T>> GetCollectionInfoFromFile<T>(string filename)
        {
            if (ObjectCache.ContainsKey(filename))
            {
                return (IList<T>)ObjectCache[filename];
            }
            else
            {
                var collection = await FileDeserializerHelper.GetCollectionInfoFromFile<T>(filename);
                ObjectCache.Add(filename, collection);
                return collection;
            }
        }

        protected void DealWithErrors(Exception ex,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string filePath = "")
        {
            LoggingService.DealWithError(ex, lineNumber, memberName, filePath);
        }
    }
}
