using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DemoMvxApp.Core.Interfaces;
using DemoMvxApp.Core.Models;

namespace DemoMvxApp.Core.Services
{
    public class MockContactService : BaseMockService, IContactsService
    {
        private const string CONTACTS_FILE = "mock_contacts.json";

        public MockContactService(ILoggingService loggingService) : base(loggingService)
        {
        }

        public async Task<IList<Contact>> GetContacts()
        {
            IList<Contact> contacts = null;
            try
            {
                contacts = await GetCollectionInfoFromFile<Contact>(CONTACTS_FILE);
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }

            return contacts;
        }
    }
}
