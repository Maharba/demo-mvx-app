using System;
using System.Runtime.CompilerServices;
using DemoMvxApp.Core.Interfaces;

namespace DemoMvxApp.Core.Services
{
    public class DebugLoggingService : ILoggingService
    {
        public void DealWithError(
            Exception ex,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string memberFilePath = "")
        {
            System.Diagnostics.Debug.WriteLine($"<<ERRROR>>:  {DateTime.Now} {memberName} :: {memberFilePath} at Line Number: {lineNumber} :: Message: {ex.Message} :: Stack Trace: {ex.StackTrace}");
        }

        public void DealWithInfo(string title, string info)
        {
            System.Diagnostics.Debug.WriteLine($"<<INFO>>:  {DateTime.Now} - {title}: {info}");
        }

        public void DealWithWarning(string title,
            string warning,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string memberFilePath = "")
        {
            System.Diagnostics.Debug.WriteLine($"<<WARNING>>:  {DateTime.Now} {memberName} :: {memberFilePath} at Line Number: {lineNumber} ::  {title}: {warning}");
        }
    }
}
