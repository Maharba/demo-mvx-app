
using System;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DemoMvxApp.Core.Models.Nodes;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace DemoMvxApp.Core.ViewModels.Contact
{
    public class ContactViewModel : BaseViewModel<Models.Contact, ContactNode>
    {
        public IMvxAsyncCommand SaveCommand { get; private set; }

        private MvxInteraction _dialogInteraction = new MvxInteraction();
        public IMvxInteraction DialogInteraction => _dialogInteraction;

        private string _passwordText;
        public string PasswordText
        {
            get => _passwordText;
            set => SetProperty(ref _passwordText, value);
        }

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        private string _lastName;

        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        private readonly IMvxNavigationService _navigationService;

        public ContactViewModel(IMvxNavigationService navigationService)
        {
            SaveCommand = new MvxAsyncCommand(SaveContact);
            _navigationService = navigationService;
        }

        private async Task SaveContact()
        {
            if (!Regex.IsMatch( PasswordText, "\\w{5,12}|\\d{5,12}") || Regex.IsMatch(PasswordText, "(.+)\\1"))
            {
                // Password does not match criteria
                _dialogInteraction.Raise();
            }
            else
            {

                await _navigationService.Close(this, new ContactNode()
                {
                    NodeType = ContactNodeType.TeamMember,
                    Value = new Models.Contact()
                    {
                        Id = RandomNumberGenerator.GetInt32(int.MinValue, int.MaxValue),
                        FirstName = this.FirstName,
                        LastName = this.LastName,
                        Password = this.PasswordText
                    }
                });
            }
        }

        // Init and Start are important parts of MvvmCross' CIRS ViewModel lifecycle
        // Learn how to use Init and Start at https://github.com/MvvmCross/MvvmCross/wiki/view-model-lifecycle
        public void Init()
        {
        }

        public override void Prepare(Models.Contact parameter) => throw new System.NotImplementedException();

        public override void Start()
        {
        }
    }
}
