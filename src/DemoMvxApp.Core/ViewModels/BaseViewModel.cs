using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.ViewModels;

namespace DemoMvxApp.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private bool _isRefreshing;
        /// <summary>
        /// This property is used to know if some collection is being refreshed
        /// </summary>
        /// <value><c>true</c> if is refreshing; otherwise, <c>false</c>.</value>
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetProperty(ref _isRefreshing, value);
        }

        protected virtual Task GetDataAsync(bool showLoadingIndicator = true)
        {
            return Task.FromResult(0);
        }

    }
}
