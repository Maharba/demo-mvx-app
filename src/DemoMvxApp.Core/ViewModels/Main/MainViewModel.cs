using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using DemoMvxApp.Core.Interfaces;
using DemoMvxApp.Core.Models.Nodes;
using DemoMvxApp.Core.ViewModels.Contact;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace DemoMvxApp.Core.ViewModels.Main
{
    public class MainViewModel : BaseViewModel, IContactsViewModel
    {
        private IContactsRepository _contactsRepository;
        private readonly IMvxNavigationService _navigationService;

        public MainViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public ObservableCollection<ContactNode> FilteredTeamMembers { get; set; }
        public IMvxAsyncCommand<ContactNode> AddContact { get; private set; }

        protected override async Task GetDataAsync(bool showLoadingIndicator = true)
        {
            var nodes = await _contactsRepository.GetContacts();
            FilteredTeamMembers = new MvxObservableCollection<ContactNode>(nodes);

            AddContact = new MvxAsyncCommand<ContactNode>(NavigateToContactView);
        }

        

        private async Task NavigateToContactView(ContactNode contactNode)
        {
            var result = await _navigationService.Navigate<ContactViewModel, ContactNode>();
            if (result != null)
            {
                FilteredTeamMembers.Add(result);
            }
        }

        public override async void Prepare()
        {
            _contactsRepository = Mvx.IoCProvider.Resolve<IContactsRepository>();
            await GetDataAsync();
        }

        
    }
}
