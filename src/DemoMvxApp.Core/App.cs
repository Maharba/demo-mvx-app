using DemoMvxApp.Core.Factories;
using DemoMvxApp.Core.Interfaces;
using DemoMvxApp.Core.Repositories;
using DemoMvxApp.Core.Services;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using DemoMvxApp.Core.ViewModels.Main;
using MvvmCross;

namespace DemoMvxApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            DataServiceFactory.Instance.SetDataServicesMock();

            RegisterAppStart<MainViewModel>();
        }
    }
}
