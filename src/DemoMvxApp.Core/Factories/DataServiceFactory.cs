using System;
using System.Collections.Generic;
using System.Text;
using DemoMvxApp.Core.Interfaces;
using DemoMvxApp.Core.Services;

namespace DemoMvxApp.Core.Factories
{
    public class DataServiceFactory
    {
        public ILoggingService LoggingService { get; set; }
        public IContactsService ContactService { get; private set; }

        public static DataServiceFactory Instance { get; } = new DataServiceFactory();
        private DataServiceFactory() { }

        public void SetDataServicesApi()
        {
            //TODO: Add api services here.
        }

        public void SetDataServicesMock()
        {
            ContactService = new MockContactService(LoggingService);
        }
    }
}
