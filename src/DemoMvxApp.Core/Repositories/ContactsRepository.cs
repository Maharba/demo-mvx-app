using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoMvxApp.Core.Interfaces;
using DemoMvxApp.Core.Models;
using DemoMvxApp.Core.Models.Nodes;

namespace DemoMvxApp.Core.Repositories
{
    public class ContactsRepository : BaseRepository, IContactsRepository
    {
        public ContactsRepository(ILoggingService loggingService) : base(loggingService)
        {
        }

        public async Task<IList<ContactNode>> GetContacts()
        {
            var nodes = new List<ContactNode>();
            try
            {
                var contacts = await DataServicesFactory.ContactService.GetContacts();
                if (contacts.Any())
                {
                    foreach (Contact contact in contacts)
                    {
                        nodes.Add(new ContactNode(ContactNodeType.TeamMember, contact));
                    }
                }
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }

            return nodes;
        }
    }
}
