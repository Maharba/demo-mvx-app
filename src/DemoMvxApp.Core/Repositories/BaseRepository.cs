using System;
using System.Runtime.CompilerServices;
using DemoMvxApp.Core.Factories;
using DemoMvxApp.Core.Interfaces;

namespace DemoMvxApp.Core.Repositories
{
    public class BaseRepository
    {
        protected readonly ILoggingService LoggingService;
        protected DataServiceFactory DataServicesFactory => DataServiceFactory.Instance;

        public BaseRepository(ILoggingService loggingService)
        {
            LoggingService = loggingService;
        }

        protected void DealWithErrors(Exception ex,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string filePath = "")
        {
            LoggingService.DealWithError(ex, lineNumber, memberName, filePath);
        }
    }
}
