using System.Collections.ObjectModel;
using DemoMvxApp.Core.Models;
using DemoMvxApp.Core.Models.Nodes;
using MvvmCross.Commands;

namespace DemoMvxApp.Core.Interfaces
{
    public interface IContactsViewModel
    {
        ObservableCollection<ContactNode> FilteredTeamMembers { get; set; }
        IMvxAsyncCommand<ContactNode> AddContact { get; }
    }
}
