using System.Collections.Generic;
using System.Threading.Tasks;
using DemoMvxApp.Core.Models;

namespace DemoMvxApp.Core.Interfaces
{
    public interface IContactsService
    {
        Task<IList<Contact>> GetContacts();
    }
}
