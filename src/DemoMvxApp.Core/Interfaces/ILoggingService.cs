using System.Runtime.CompilerServices;

namespace DemoMvxApp.Core.Interfaces
{
    public interface ILoggingService
    {
        void DealWithInfo(string title, string info);

        void DealWithWarning(string title,
            string warning,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string memberFilePath = ""
        );

        void DealWithError(System.Exception ex,
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string memberFilePath = ""
        );
    }
}
