using System.Collections.Generic;
using System.Threading.Tasks;
using DemoMvxApp.Core.Models;
using DemoMvxApp.Core.Models.Nodes;

namespace DemoMvxApp.Core.Interfaces
{
    public interface IContactsRepository
    {
        Task<IList<ContactNode>> GetContacts();
    }
}
