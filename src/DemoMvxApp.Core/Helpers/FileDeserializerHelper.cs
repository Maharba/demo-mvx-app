using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using DemoMvxApp.Core.Services;
using Newtonsoft.Json;

namespace DemoMvxApp.Core.Helpers
{
    public class FileDeserializerHelper
    {
        public static Dictionary<string, string> FileCache { get; set; }

        public static async Task<List<T>> GetCollectionInfoFromFile<T>(string filename)
        {
            List<T> listResults = new List<T>();
            try
            {
                var isCached = false;
                var json = string.Empty;

                if (FileCache == null)
                {
                    FileCache = new Dictionary<string, string>();
                }

                if (FileCache.ContainsKey(filename))
                {
                    json = FileCache[filename];
                    isCached = true;
                }
                else
                {
                    json = await GetResourceString(filename);
                    isCached = false;
                }
                if (!string.IsNullOrEmpty(json))
                {
                    if (!isCached)
                    {
                        FileCache[filename] = json;
                    }
                    var results = JsonConvert.DeserializeObject<List<T>>(json);
                    if (results != null && results.Count > 0)
                    {
                        listResults = results;
                    }
                }
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }
            return listResults;
        }

        public static void UpdateTeamAccess<T>(List<T> teams, String filename)
        {
            try
            {
                var json = string.Empty;

                json = JsonConvert.SerializeObject(teams);
                if (!string.IsNullOrEmpty(json))
                {
                    FileCache[filename] = json;
                }
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }
        }

        public static async Task<bool> UpdateResouceFileByObject<T>(T objectModel, string resourceFile)
        {
            bool isSucced = false;
            try
            {
                var json = string.Empty;
                json = JsonConvert.SerializeObject(objectModel);
                if (!string.IsNullOrEmpty(json))
                {
                    //update cache
                    FileCache[resourceFile] = json;
                }
                isSucced = true;
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }
            return isSucced;
        }

        public static async Task<string> GetResourceString(string resourceName)
        {
            string results = string.Empty;
            Stream stream = null;
            StreamReader sr = null;
            var assembly = typeof(BaseMockService).GetTypeInfo().Assembly;

            var resourcePath = $"DemoMvxApp.Core.Assets.{resourceName}";
            try
            {
                stream = assembly.GetManifestResourceStream(resourcePath);
                if (stream != null && stream.Length > 0)
                {
                    sr = new StreamReader(stream);
                    results = await sr.ReadToEndAsync();
                }
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
            }
            finally
            {
                sr.Dispose();
                sr = null;

                stream.Dispose();
                stream = null;
            }
            return results;
        }

        private static void DealWithErrors(Exception ex)
        {
            System.Diagnostics.Debug.WriteLine($"Error: {ex.Message} - Stack Trace: {ex.StackTrace}");
        }

        public static async Task<T> SerializeJsonFromFile<T>(string filename)
        {
            T result = default(T);
            try
            {
                var isCached = false;
                var json = string.Empty;

                if (FileCache == null)
                {
                    FileCache = new Dictionary<string, string>();
                }

                if (FileCache.ContainsKey(filename))
                {
                    json = FileCache[filename];
                    isCached = true;
                }
                else
                {
                    json = await GetResourceString(filename);
                    isCached = false;
                }
                if (!string.IsNullOrEmpty(json))
                {
                    if (!isCached)
                    {
                        FileCache[filename] = json;
                    }
                    result = JsonConvert.DeserializeObject<T>(json);
                }
            }
            catch (Exception ex)
            {
                DealWithErrors(ex);
                throw ex;
            }
            return result;
        }
    }
}
