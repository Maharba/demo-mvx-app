# README #

This small application is for demonstration purposes only.

### What is this repository for? ###

* A technical demonstration for cross-platform application development while still mantaining UI customization with MvvmCross in Xamarin.
* A very simple main UI that displays a list of users and their information.
* A view that adds new users to the list. The user's password must comply with the following rules:
	* Password must consist of a mixture of letters and numerical digits only, with at least one of each.
	* Password must be between 5 and 12 characters in length.
	* Password must not contain any sequence of characters immediately followed by the same sequence.
* A simple alert dialog letting the user know that the password doesn't comply with the above rules.

### What is in this repository ###

* Two projects: DemoMvxApp.Core and DemoMvxApp.Droid
	* **DemoMvxApp.Core** contains business logic, services, helpers, factories, models, view models, interfaces and repositories. Mock data was provided for initial data.
	* **DemoMvxApp.Droid** contains all UI for Android, resources and template selectors in case there are more than one data type in the list for future references.
	
The purpose for building the app with this architecture is to demonstrate the capabilities of MvvmCross as a solution to optimize code and time in case another supported platform will be in the roadmap.